package model;

/**
 *
 * @author Herminio
 */
public enum IrisAttributesIndex {
    LARGO_SEPALO (0),
    ANCHO_SEPALO (1),
    LARGO_PETALO (2),
    ANCHO_PETALO (3),
    CLASS (4);

    private int index;

    IrisAttributesIndex(int index) {
        this.index = index;
    }

    public static IrisAttributesIndex newInstance(int index) {
        for (IrisAttributesIndex weatherAttributesIndex : IrisAttributesIndex.values()) {
            if (index == weatherAttributesIndex.index) {
                return weatherAttributesIndex;
            }
        }
        throw new IllegalArgumentException("Indice no encontrado.");
    }

    public int getIndex() {
        return index;
    }
    
}
