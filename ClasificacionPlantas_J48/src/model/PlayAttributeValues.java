/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Herminio
 */
public enum PlayAttributeValues {
    IRIS_SETOSA (0),
    IRIS_VERSICOLOR (1),
    IRIS_VIRGINICA (2);

    private int index;

    PlayAttributeValues(int index) {
        this.index = index;
    }

    public static PlayAttributeValues newInstance(int index) {
        for (PlayAttributeValues playAttributeValues : PlayAttributeValues.values()) {
            if (index == playAttributeValues.index) {
                return playAttributeValues;
            }
        }
        throw new IllegalArgumentException("Indice no encontrado");
    }

    public int getIndex() {
        return index;
    }
}
